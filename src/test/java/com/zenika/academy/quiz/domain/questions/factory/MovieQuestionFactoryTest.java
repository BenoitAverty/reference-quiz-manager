package com.zenika.academy.quiz.domain.questions.factory;

import com.zenika.academy.quiz.domain.questions.Question;
import com.zenika.academy.quiz.services.factory.MovieQuestionFactory;
import com.zenika.academy.quiz.services.factory.QuestionLevel;
import com.zenika.academy.quiz.services.factory.RandomMovieElementsGenerator;
import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.Year;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static com.zenika.academy.quiz.domain.questions.AnswerResult.CORRECT;
import static java.lang.System.lineSeparator;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

class MovieQuestionFactoryTest {

    private TmdbClient tmdbMock;
    private MovieQuestionFactory factory;
    private RandomMovieElementsGenerator randomGeneratorMock;

    @BeforeEach
    void setup() {
        this.tmdbMock = Mockito.mock(TmdbClient.class);
        this.randomGeneratorMock = Mockito.mock(RandomMovieElementsGenerator.class);
        this.factory = new MovieQuestionFactory(
                this.tmdbMock,
                this.randomGeneratorMock,
                new Random(1)
        );
    }

    @Test
    void testWithNoMovie() {
        Mockito.when(this.tmdbMock.getMovie(anyString())).thenReturn(Optional.empty());

        Optional<Question> actual = this.factory.createMovieQuestion("NoMovieHasThatName", QuestionLevel.EASY);

        Assertions.assertTrue(actual.isEmpty());
    }

    @Test
    void testHardAndMediumMovieQuestion() {
        MovieInfo fakeMovie = new MovieInfo(
                "Titanic",
                Year.of(1997),
                List.of(
                        new MovieInfo.Character("Rose Dewitt", "Kate Winslet"),
                        new MovieInfo.Character("Jack Dawson", "Leonardo DiCaprio"),
                        new MovieInfo.Character("Cal Hockey", "Billy Zane")
                )
        );

        Mockito.when(this.tmdbMock.getMovie("Titanic")).thenReturn(Optional.of(fakeMovie));

        Optional<Question> actualMedium = this.factory.createMovieQuestion("Titanic", QuestionLevel.MEDIUM);
        Optional<Question> actualHard = this.factory.createMovieQuestion("Titanic", QuestionLevel.HARD);

        Assertions.assertTrue(actualMedium.isPresent());
        Assertions.assertEquals("Quel film sorti en 1997 contient des personnages joué par les acteurs suivants : Kate Winslet, Leonardo DiCaprio, Billy Zane ?", actualMedium.get().getDisplayableText());
        Assertions.assertEquals(CORRECT, actualMedium.get().tryAnswer("Titanic"));
        Assertions.assertTrue(actualHard.isPresent());
        Assertions.assertEquals("Quel film sorti en 1997 contient des personnages joué par les acteurs suivants : Kate Winslet, Leonardo DiCaprio, Billy Zane ?", actualHard.get().getDisplayableText());
        Assertions.assertEquals(CORRECT, actualHard.get().tryAnswer("Titanic"));
    }

    @Test
    void testEasyMovieQuestion() {
        MovieInfo fakeMovie = new MovieInfo(
                "Titanic",
                Year.of(1997),
                List.of(
                        new MovieInfo.Character("Rose Dewitt", "Kate Winslet"),
                        new MovieInfo.Character("Jack Dawson", "Leonardo DiCaprio"),
                        new MovieInfo.Character("Cal Hockey", "Billy Zane")
                )
        );

        Mockito.when(this.tmdbMock.getMovie("Titanic")).thenReturn(Optional.of(fakeMovie));
        Mockito.when(this.randomGeneratorMock.getRandomMovieNames(anyInt(), anyString()))
                .thenReturn(List.of("Inglourious Basterds", "Dirty Dancing"));

        Optional<Question> actual = this.factory.createMovieQuestion("Titanic", QuestionLevel.EASY);

        Assertions.assertTrue(actual.isPresent());
        Assertions.assertEquals(
                "Quel film sorti en 1997 contient des personnages joué par les acteurs suivants : Kate Winslet, Leonardo DiCaprio, Billy Zane ?" + lineSeparator() +
                        "  1) Dirty Dancing" + lineSeparator() +
                        "  2) Titanic" + lineSeparator() +
                        "  3) Inglourious Basterds" + lineSeparator(),
                actual.get().getDisplayableText());
        Assertions.assertEquals(CORRECT, actual.get().tryAnswer("Titanic"));
    }
}