create table if not exists quizzes
(
    id   text primary key,
    name text not null
);

create table if not exists questions
(
    id            text primary key,
    quiz_id       text references quizzes (id),
    question_text text not null,
    open_answer   text,
    true_false_answer bool
);

create table if not exists suggestions
(
    question_id text not null references questions,
    suggestion_text text not null
);