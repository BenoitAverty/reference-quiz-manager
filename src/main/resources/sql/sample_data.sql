insert into quizzes (id, name) values ('bc357f75-3038-4522-8ded-e5a248eccd87', 'Films populaires');
insert into quizzes (id, name) values ('29bce22c-0630-42b4-a830-b6ea9613f5db', 'Bande dessinée');

-- Insert some open questions
insert into questions (id, quiz_id, question_text, open_answer) values ('6568ae66-7c44-47b3-b15b-e16c6d5aeb6f', 'bc357f75-3038-4522-8ded-e5a248eccd87', 'Quel film sorti en 2006 contient des personnages joué par les acteurs suivants : Christian Bale, Hugh Jackman, Michael Caine ?', 'The Prestige');
insert into questions (id, quiz_id, question_text, open_answer) values ('ddc5a7d6-8b1a-48b8-ab7a-0806d143ca2a', '29bce22c-0630-42b4-a830-b6ea9613f5db', 'Comment s''appelle le chien d''Obélix ?', 'Idéfix');

-- insert some multiple choice questions
insert into questions (id, quiz_id, question_text, open_answer) values ('c7fd7431-28fd-4167-9afa-90d7629044c3', 'bc357f75-3038-4522-8ded-e5a248eccd87', 'Quel film sorti en 2009 contient des personnages joué par les acteurs suivants : Brad Pitt, Mélanie Laurent, Christoph Waltz ?', 'Inglourious Basterds');
insert into suggestions (question_id, suggestion_text) values ('c7fd7431-28fd-4167-9afa-90d7629044c3', 'Spring, Summer, Fall... and Spring');
insert into suggestions (question_id, suggestion_text) values ('c7fd7431-28fd-4167-9afa-90d7629044c3', 'Double Indemnity');

insert into questions (id, quiz_id, question_text, open_answer) values ('be8b2923-2560-4789-bff6-d13d6a5a07d8', '29bce22c-0630-42b4-a830-b6ea9613f5db', 'Comment s''appelle la tortue dans Boule et Bill ?', 'Caroline');
insert into suggestions (question_id, suggestion_text) values ('be8b2923-2560-4789-bff6-d13d6a5a07d8', 'Boule');
insert into suggestions (question_id, suggestion_text) values ('be8b2923-2560-4789-bff6-d13d6a5a07d8', 'Bill');
insert into suggestions (question_id, suggestion_text) values ('be8b2923-2560-4789-bff6-d13d6a5a07d8', 'Tortue Géniale');

-- insert some true/false questions
insert into questions (id, quiz_id, question_text, true_false_answer) values ('feb83aeb-05ee-4a27-931b-fb8a8af0a0b3', 'bc357f75-3038-4522-8ded-e5a248eccd87', 'Le personnage joué par Christian Bale dans le film The Prestige s''appelle Alfred Borden', true);
insert into questions (id, quiz_id, question_text, true_false_answer) values ('9a34d227-725f-445f-b3f2-1e1f7d0a036e', '29bce22c-0630-42b4-a830-b6ea9613f5db', 'Milou est en réalité un chat', false);
