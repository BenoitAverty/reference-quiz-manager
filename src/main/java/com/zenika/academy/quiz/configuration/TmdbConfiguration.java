package com.zenika.academy.quiz.configuration;

import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.io.IOException;

@Configuration
public class TmdbConfiguration {

    @Value("classpath:tmdb-api-key.txt")
    private Resource tmdbApiKey;

    @Bean
    public TmdbClient tmdbClient() throws IOException {
        return new TmdbClient(this.tmdbApiKey.getInputStream());
    }
}
