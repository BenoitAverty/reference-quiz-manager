package com.zenika.academy.quiz.infrastructure;

import com.zenika.academy.quiz.domain.questions.MultipleChoiceQuestion;
import com.zenika.academy.quiz.domain.questions.OpenQuestion;
import com.zenika.academy.quiz.domain.questions.Question;
import com.zenika.academy.quiz.domain.questions.TrueFalseQuestion;
import com.zenika.academy.quiz.repositories.QuestionRepository;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Component
public class JdbcQuestionRepository implements QuestionRepository {
    private JdbcTemplate jdbcTemplate;
    private ResultSetExtractor<Question> oneQuestionExtractor;
    private ResultSetExtractor<List<Question>> severalQuestionsExtractor;

    public JdbcQuestionRepository(JdbcTemplate jdbcTemplate, QuestionExtractor questionExtractor) {
        this.jdbcTemplate = jdbcTemplate;
        this.oneQuestionExtractor = questionExtractor::buildOneQuestion;
        this.severalQuestionsExtractor = questionExtractor::buildSeveralQuestions;
    }

    @Override
    public List<Question> getAll() {
        return this.getQuestions(null);
    }

    @Override
    public List<Question> getByQuizId(String quizId) {
        return this.getQuestions(quizId);
    }

    private List<Question> getQuestions(String quizId) {
        String whereClause = quizId != null ? " where questions.quiz_id = ?" : "";
        Object[] params = quizId != null ? new String[]{quizId} : new String[]{};

        return this.jdbcTemplate.query(
                "select * \n" +
                        "from questions left outer join suggestions \n" +
                        "    " +
                        "on questions.id = suggestions.question_id " + whereClause, this.severalQuestionsExtractor, params);
    }

    @Override
    @Transactional
    public void save(Question q) {
        if (q instanceof OpenQuestion) {
            this.jdbcTemplate.update(
                    "insert into questions (id, quiz_id, question_text, open_answer) VALUES (?, ?, ?, ?)",
                    q.getId(),
                    q.getQuizId(),
                    q.getDisplayableText(),
                    ((OpenQuestion) q).getCorrectAnswer()
            );
        } else if (q instanceof TrueFalseQuestion) {
            this.jdbcTemplate.update(
                    "insert into questions (id, quiz_id, question_text, true_false_answer) VALUES (?, ?, ?, ?)",
                    q.getId(),
                    q.getQuizId(),
                    ((TrueFalseQuestion) q).getText(),
                    ((TrueFalseQuestion) q).getCorrectAnswer()
            );
        } else if (q instanceof MultipleChoiceQuestion) {
            this.jdbcTemplate.update(
                    "insert into questions (id, quiz_id, question_text, open_answer) VALUES (?, ?, ?, ?)",
                    q.getId(),
                    q.getQuizId(),
                    ((MultipleChoiceQuestion) q).getText(),
                    ((MultipleChoiceQuestion) q).getCorrectAnswer()
            );
            ((MultipleChoiceQuestion) q).getSuggestions().forEach(s -> {
                this.jdbcTemplate.update("insert into suggestions (question_id, suggestion_text) VALUES (?, ?)", q.getId(), s);
            });

        }
    }

    @Override
    public Optional<Question> getOne(String id) {
        final Question question = this.jdbcTemplate.query("select *\n" +
                "from questions\n" +
                "    left outer join suggestions s on questions.id = s.question_id\n" +
                "where questions.id = ?", this.oneQuestionExtractor, id);
        return Optional.ofNullable(question);
    }
}
