package com.zenika.academy.quiz.repositories;

import com.zenika.academy.quiz.domain.questions.Question;

import java.util.List;
import java.util.Optional;

public interface QuestionRepository {

    List<Question> getAll();
    List<Question> getByQuizId(String quizId);
    void save(Question q);
    Optional<Question> getOne(String id);
}
