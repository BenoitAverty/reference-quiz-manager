package com.zenika.academy.quiz.repositories;

import com.zenika.academy.quiz.domain.Quiz;

import java.util.List;
import java.util.Optional;

public interface QuizRepository {
    List<Quiz> getAll();
    void save(Quiz q);
    Optional<Quiz> getOne(String id);
}
